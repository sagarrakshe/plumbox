import java.io.*;
import java.util.stream.Stream;
import net.sourceforge.argparse4j.inf.*;

import org.json.JSONObject;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class CommandExecutorTest {

    private final ByteArrayOutputStream outContent = new ByteArrayOutputStream();
    private final ByteArrayOutputStream errContent = new ByteArrayOutputStream();

    @Before
    public void setUpStreams() {
        System.setOut(new PrintStream(outContent));
        System.setErr(new PrintStream(errContent));
    }

    @After
    public void restoreStreams() {
        System.setOut(System.out);
        System.setErr(System.err);
    }

    @Test
    public void testDeclareFact() {
        String[] args = Stream.of("declare-fact",
                "--source", "SOURCE",
                "--sink", "SINK",
                "--input_table", "INPUT_TABLES",
                "--output_table", "OUTPUT_TABLE",
                "--driver", "DRIVER").toArray(String[]::new);

        // TODO: Remove the host hard-coding
        String host = "192.168.33.10";
        CommandExecutor executor;

        try {

            executor = new CommandExecutor();
            executor.execute(args);

            //Redirect to console output
            /*
            PrintStream consoleStream = new PrintStream(
                        new FileOutputStream(FileDescriptor.out));
            System.setOut(consoleStream);
            */

            String key = outContent.toString().split(":")[1].trim();
            RediStorage redis = new RediStorage(host);
            boolean result = redis.doesExist(key);

            assertEquals(true, result);

        } catch (ArgumentParserException e) {
                e.printStackTrace();
            }
    }
}